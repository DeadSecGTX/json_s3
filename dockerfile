FROM node:10 AS builder
WORKDIR /usr/index.js
COPY ./package*.json ./
RUN yarn add index.js
COPY . .

FROM node:10-alpine
WORKDIR /usr/index.js
COPY --from=builder /usr/index.js ./
EXPOSE 3000
CMD ["yarn", "dev"]