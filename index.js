require('dotenv').config();

const express = require('express');
const bodyparser = require('body-parser');
const mongoose = require('mongoose');

const app = express();

mongoose.connect('mongodb://ec2-52-86-207-245.compute-1.amazonaws.com:17017/uploads', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
})

app.use(express.json());

app.use(bodyparser.json());

app.use(express.urlencoded({ extended: true }));

app.use(require('./routes'));

app.listen(3000, function()
{
    console.log("Servidor rodando com express");
});









